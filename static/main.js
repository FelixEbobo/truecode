
// document.getElementById("testButton").addEventListener("click", function(){
// 	fetch('/test', {
// 		method: "POST",
// 		headers: {
// 			'Content-Type': 'application/json'
// 		}
// 	}).then((response) => 
// 		response.json()
// 	).then((data) => {
// 		console.log('Success:', data)
// 		let link_place = document.querySelector("#yandexLink")
// 		link_place.innerHTML = "Ссылка на папку: " + data.url
// 	}).catch((error) => {
// 		console.log('Error: ', error)
// 	})

// });
function handleForm(event) { event.preventDefault(); } 
document.getElementById("myForm").addEventListener('submit', handleForm);

(function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
	.forEach(function (form) {
	  form.addEventListener('submit', function (event) {
		if (!form.checkValidity()) {
		  event.preventDefault()
		  event.stopPropagation()
		}

		form.classList.add('was-validated')
	  }, false)
	})
})()

document.getElementById("generateReport").addEventListener("click", function(){
	const from_date_value = document.querySelector("#from_date").value
	const to_date_value = document.querySelector("#to_date").value
	if (from_date_value === "" || to_date_value === "") {
		return
	}
	const from_date = new Date(from_date_value)
	const to_date = new Date(to_date_value)

	if (to_date - from_date < 0) {
		alert("Местами поменяйте :)")
	}

	const data = {
		from_date: from_date_value,
		to_date: to_date_value
	}
	document.getElementById("spinnerBox").style["display"] = "flex";

	fetch('/generate', {
		method: "POST",
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	}).then((response) => 
		response.json()
	).then((data) => {
		console.log('Success:', data)
		console.log(data.url)
		let link_place = document.querySelector("#yandexLink")
		link_place.innerHTML = 'Ссылка на папку: ' + data.url
		link_place.href = data.url
	}).catch((error) => {
		console.log('Error: ', error)
		let link_place = document.querySelector("#yandexLink")
		link_place.innerHTML = "Произошла ошибка: " + error
	}).finally(() => {
		document.getElementById("spinnerBox").style["display"] = "none";
	})

	
});