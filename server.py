import os
from flask import Flask, render_template, request, jsonify
from datetime import datetime
from bitrix_collector import BitrixCollector
from excel_writer import ExcelCreator
from yandex_api import create_folder, publish_folder, put_report, get_public_url

SERVER_PORT = 9545

app = Flask(__name__)
app.debug = True

@app.route('/')
def index():
	return render_template('index.html')


@app.route('/generate', methods=['POST'])
def generate():
	from_date = request.json['from_date']
	to_date = request.json['to_date']

	app = BitrixCollector(from_date, to_date)
	app.execute()
	first_report_name = f'human_report_{from_date}_to_{to_date}.xlsx'
	ExcelCreator().create_human_report('file.json', first_report_name)
	second_report_name = f'analytic_report_{from_date}_to_{to_date}.xlsx'
	ExcelCreator().create_analytics_report('file.json', second_report_name)

	date = 'Отчёты_от_' + datetime.now().strftime('%d-%m-%Y_%H-%m-%S')
	create_folder(date)
	put_report(first_report_name, date)
	put_report(second_report_name, date)
	publish_folder(date)
	url = get_public_url(date)
	os.remove(first_report_name)
	os.remove(second_report_name)

	return jsonify({'url': url})

@app.route('/test', methods=['POST'])
def test():
	return jsonify({'url': 'dasfjadsfh'})


if __name__ == '__main__':
	app.run(port=SERVER_PORT)