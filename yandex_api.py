import http
import requests

YANDEX_TOKEN = 'y0_AgAEA7qkROuqAAhrogAAAADRC30PO17UqDNBTsm1rMUaNSHGIpAkeLA'
HEADERS = {'Authorization': f'OAuth {YANDEX_TOKEN}'}

def create_folder(folder: str):
	resp = requests.put('https://cloud-api.yandex.net/v1/disk/resources?path=reports', headers=HEADERS)
	if resp.status_code != http.HTTPStatus.OK and resp.status_code != http.HTTPStatus.CONFLICT:
		raise Exception(resp.content)
	print(f'creating folder {folder}')
	resp = requests.put(f'https://cloud-api.yandex.net/v1/disk/resources?path=reports/{folder}&fields=name', headers=HEADERS)
	if resp.status_code != http.HTTPStatus.CREATED:
		raise Exception(resp.content)

def put_report(report: str, folder: str):
	print(f'getting upload link for {report}')
	resp = requests.get(f'https://cloud-api.yandex.net/v1/disk/resources/upload?path=reports/{folder}/{report}', headers=HEADERS)
	if resp.status_code != http.HTTPStatus.OK:
		raise Exception(resp.content)
	with open(report, 'rb') as file:
		print('uploading file')
		resp = requests.put(resp.json()['href'], headers=HEADERS, files={'file': file})
		if resp.status_code not in (http.HTTPStatus.CREATED, http.HTTPStatus.ACCEPTED):
			raise Exception(resp.content)

def publish_folder(folder: str) -> str:
	print(f'publishing folder {folder}')
	resp = requests.put(f'https://cloud-api.yandex.net/v1/disk/resources/publish?path=reports/{folder}', headers=HEADERS)
	if resp.status_code != http.HTTPStatus.OK:
		raise Exception(resp.content.decode('utf-8'))
	return resp.json()['href']

def get_public_url(folder: str) -> str:
	resp = requests.get('https://cloud-api.yandex.net/v1/disk/resources/public', headers=HEADERS)
	if resp.status_code != http.HTTPStatus.OK:
		raise Exception(resp.content.decode('utf-8'))
	for item in resp.json()['items']:
		if item['name'] == folder:
			return item['public_url']