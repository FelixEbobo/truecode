import json
from typing import Dict
import openpyxl as xl
import time
import re
from datetime import datetime
from openpyxl.workbook.child import INVALID_TITLE_REGEX
from openpyxl.styles import Font, Alignment
from openpyxl.utils import get_column_letter

def cell_center_and_bold(cell):
	return cell_bold(cell_center(cell, True))

def cell_bold(cell):
	cell.font = Font(bold=True)
	return cell

def cell_center(cell, wrap_text=False):
	cell.alignment = Alignment(horizontal='center', wrap_text=wrap_text)
	return cell

class ExcelCreator:
	def __init__(self) -> None:
		self.row = 1

		self.time_row = 5

		self.workbook = xl.Workbook()
		self.workbook.remove(self.workbook.worksheets[0])
		self.cur_ws = None # Текущий worksheet

	@staticmethod
	def convert_seconds_to_time(seconds: int):
		return datetime.fromtimestamp(time.mktime(time.gmtime(seconds))).time()

	def organise_titles(self):
		self.cur_ws.row_dimensions[self.row].height = 30

		cell_center_and_bold(self.cur_ws.cell(self.row, 1, value='Задача'))
		cell_center_and_bold(self.cur_ws.cell(self.row, 2, value='Планируемое время (часы)'))
		cell_center_and_bold(self.cur_ws.cell(self.row, 3, value='Фактическое время (часы)'))
		cell_center_and_bold(self.cur_ws.cell(self.row, 4, value='До (дата)'))

		cell_center_and_bold(self.cur_ws.cell(self.row + 2, 1, value='Затраты'))
		self.cur_ws.merge_cells(start_row=self.row + 2, start_column=1, end_row=self.row + 2, end_column=4)
		cell_center_and_bold(self.cur_ws.cell(self.row + 3, 1, value='Исполнитель'))
		cell_center_and_bold(self.cur_ws.cell(self.row + 3, 2, value='Время'))
		cell_center_and_bold(self.cur_ws.cell(self.row + 3, 3, value='Дата'))

	def add_main_task_details(self, task: Dict):
		def __calculate_time(create_date: str, due_date: str) -> int:
			if not due_date:
				return 'N/A'
			return ((datetime.strptime(due_date, "%Y-%m-%d") - datetime.strptime(create_date, "%Y-%m-%d")).days + 1) * 6

		self.cur_ws.cell(self.row + 1, 1, value=task['name'])

		# cell_center(self.cur_ws.cell(self.row + 1, self.column + 1, value=task['created_date']))
		due_date = 'N/A' if not task['due_date'] else task['due_date']
		cell_center(self.cur_ws.cell(self.row + 1, 2, value=__calculate_time(task['created_date'], task['due_date'])))
		cell_center(self.cur_ws.cell(self.row + 1, 4, value=due_date))

	def add_task_time_details(self, time: Dict):
		self.cur_ws.cell(self.time_row, 1, value=time['full_name'])
		cell_center(self.cur_ws.cell(self.time_row, 2, value=self.convert_seconds_to_time(time['time'])))
		cell_center(self.cur_ws.cell(self.time_row, 3, value=time['date']))

	def add_style_to_ws(self):
		self.cur_ws.column_dimensions[get_column_letter(1)].width = 40
		self.cur_ws.column_dimensions[get_column_letter(2)].width = 15
		self.cur_ws.column_dimensions[get_column_letter(3)].width = 15
		self.cur_ws.column_dimensions[get_column_letter(4)].width = 15

	def add_formula(self):
		cell_center(self.cur_ws.cell(self.row + 1, 3, f'=SUM($B${self.row + 4}:$B${self.time_row - 1})')).number_format='[hh]:mm:ss'

	def __shift_time_row(self):
		self.time_row += 1

	def __shift_row(self):
		self.row += 1

	def __shift_task_row(self):
		self.row += (self.time_row - self.row) + 2
		self.time_row += 6
	
	def __reset_rows(self):
		self.row = 1
		self.time_row = 5

	def foldable(self):
		self.cur_ws.row_dimensions.group(self.row, self.time_row, outline_level=1)

	def create_analytics_report(self, filename: str, report_name: str) -> str:
		with open(filename, 'r', encoding='utf-8') as f:
			json_str = json.load(f)

		for sheet in json_str:
			self.cur_ws = self.workbook.create_sheet()

			title = re.sub(INVALID_TITLE_REGEX, '', sheet['project_name'])

			self.cur_ws.title = title
			cell_center_and_bold(self.cur_ws.cell(self.row, 1, value='Сотрудник'))
			cell_center_and_bold(self.cur_ws.cell(self.row, 2, value='Задача'))
			cell_center_and_bold(self.cur_ws.cell(self.row, 3, value='Дата'))
			cell_center_and_bold(self.cur_ws.cell(self.row, 4, value='Время'))

			sorted_data = {}
			for task in sheet['tasks']:
				for time in task['time']:
					if not sorted_data.get(time['full_name']):
						sorted_data[time['full_name']] = []
					time['name'] = task['name']
					sorted_data[time['full_name']].append(time)

			for emlpoyee, chunk in sorted_data.items():
				for track in chunk:
					self.__shift_row()
					self.cur_ws.cell(self.row, 1, value=emlpoyee)
					cell_center(self.cur_ws.cell(self.row, 2, value=track['name']))
					cell_center(self.cur_ws.cell(self.row, 3, value=track['date']))
					cell_center(self.cur_ws.cell(self.row, 4, value=self.convert_seconds_to_time(track['time'])))

			self.cur_ws.auto_filter.ref = f'{get_column_letter(1)}1:{get_column_letter(4)}{self.row}'
			self.cur_ws.auto_filter.add_filter_column(0, [])

			cell_bold(self.cur_ws.cell(self.row + 1, 1, value=f'Итого:'))
			cell_center_and_bold(self.cur_ws.cell(self.row + 1, 4, value=f'=SUM(D2:D{self.row})')).number_format = '[hh]:mm:ss'

			self.cur_ws.column_dimensions[get_column_letter(1)].width = 15
			self.cur_ws.column_dimensions[get_column_letter(2)].width = 40
			self.cur_ws.column_dimensions[get_column_letter(3)].width = 15
			self.cur_ws.column_dimensions[get_column_letter(4)].width = 15
			self.__reset_rows()

		self.workbook.save(report_name)
		return 


	def create_human_report(self, filename: str, report_name: str):
		with open(filename, 'r', encoding='utf-8') as f:
			json_str = json.load(f)

		for sheet in json_str:
			self.cur_ws = self.workbook.create_sheet()

			title = re.sub(INVALID_TITLE_REGEX, '', sheet['project_name'])

			self.cur_ws.title = title
			for task in sheet['tasks']:
				self.organise_titles()
				self.add_main_task_details(task)
				for time in task['time']:
					self.add_task_time_details(time)
					self.__shift_time_row()
				self.add_formula()
				self.foldable()

				if len(task['time']) > 0:
					self.__shift_task_row()
			self.__reset_rows()
			self.add_style_to_ws()
		self.workbook.save(report_name)
