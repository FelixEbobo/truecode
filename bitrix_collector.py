from datetime import datetime, timedelta
import http
import requests
import json
from math import ceil, floor

WEBHOOK = '2nr3w6ka3n4oxt5s'
BITRIX_URL = 'https://truecode.bitrix24.ru/rest/611/'
BATCH_HOOK = 'batch'
GET_TASKS='tasks.task.list'
GET_TASK_HISTORY_ID='task.elapseditem.getlist.json'
GET_TASK_HISTORY = 'tasks.task.history.list'
GET_PROJECTS='sonet_group.get.json'
GET_USERS='user.get.json'

def format_utc_date(bitrix_date: str) -> str:
	# 2020-10-27T09:50:33+08:00
	if bitrix_date is None:
		return None
	return str(datetime.strptime(bitrix_date, "%Y-%m-%dT%H:%M:%S%z").date())

def format_new_date(bitrix_date: str) -> str:
	return str(datetime.strptime(bitrix_date, "%d.%m.%Y %H:%M:%S").date())

class BitrixCollector:
	def __init__(self, from_date: str, to_date: str) -> None:
		self.data = {}
		self.project_id2name = {}
		self.user_id2info = {}
		self.output = []
		self.task_time = {}
		self.tasks = []
		self.task_data = {}

		# Расширяем рамки в пределах дня, так как elapsedItem не включает эти данные
		self.from_date = str((datetime.strptime(from_date, "%Y-%m-%d") - timedelta(days=1)).date())
		self.to_date = str((datetime.strptime(to_date, "%Y-%m-%d") + timedelta(days=1)).date())

	@staticmethod
	def form_url(function: str, query: str = ""):
		return f'{BITRIX_URL}{WEBHOOK}/{function}?{query}'

	def determine_task_ids(self):
		url = self.form_url(GET_TASK_HISTORY_ID, f'order[TASK_ID]=asc&filter[>CREATED_DATE]={self.from_date}&filter[<CREATED_DATE]={self.to_date}&select[]=ID&select[]=TASK_ID')
		response = requests.get(url)
		if response.status_code != http.HTTPStatus.OK:
			raise Exception(response.content.decode('utf-8'))
		data = response.json()
		self.min_id = int(data['result'][0]['TASK_ID'])
		url += '&params[NAV_PARAMS][iNumPage]=' + str(ceil(data['total'] / 50))
		response = requests.get(url)
		if response.status_code != http.HTTPStatus.OK:
			raise Exception(response.content.decode('utf-8'))
		data = response.json()
		self.max_id = int(data['result'][-1]['TASK_ID'])

	def get_tasks(self):
		def __determine_batch_rounds() -> int:
			url = self.form_url(GET_TASKS, f'order[id]=asc&filter[>id]={self.min_id - 1}&filter[<id]={self.max_id + 1}&select[]=id')
			response = requests.get(url)
			if response.status_code != http.HTTPStatus.OK:
				print(response.content.decode('utf-8'))
				raise Exception(GET_TASKS)
			return ceil(response.json()['total'] / 50)

		payload = {}
		rounds = __determine_batch_rounds()
		print(rounds)
		for counter in range(rounds):
			payload[f'task{counter}'] = f'tasks.task.list?&filter[>id]={self.min_id}&filter[<id]={self.max_id}&order[id]=asc&start={50 * counter}&select[]=ID&select[]=TITLE&select[]=DEADLINE&select[]=CREATED_DATE&select[]=GROUP_ID'

			if (counter + 1) % 50 == 0 or counter == rounds - 1:
				response = requests.post(self.form_url(BATCH_HOOK), json={"cmd": payload})
				if response.status_code != http.HTTPStatus.OK:
					raise Exception(response.content.decode('utf-8'))
				batch_result = response.json()['result']['result']
				self._parse_tasks(batch_result)
				print(batch_result)
				payload = {}

	def _parse_tasks(self, batch_result):
		for name, chunk in batch_result.items():
			if not name.startswith('task'):
				break
			for task in chunk['tasks']:
				project_id = int(task['groupId'])
				if project_id == 0:
					continue
				if not self.data.get(project_id):
					self.data[project_id] = []
				self.project_id2name[project_id] = task['group']['name']
				self.data[project_id].append({
					'id': int(task['id']),
					'name': task['title'],
					'due_date': format_utc_date(task.get('deadline')),
					'created_date': format_utc_date(task['createdDate']),
					'time': []
				})
				self.tasks.append(int(task['id']))

	def get_task_work_time(self):
		# tasks.task.history.list?taskId=36089&filter[field]=TIME_SPENT_IN_LOGS
		# tasks.task.history.list?taskId=36089&order[CREATED_DATE]=asc&filter[field]=TIME_SPENT_IN_LOGS
		history_from_date = (datetime.strptime(self.from_date, '%Y-%m-%d') + timedelta(1)).strftime('%d.%m.%Y')
		history_to_date = (datetime.strptime(self.to_date, '%Y-%m-%d') - timedelta(1)).strftime('%d.%m.%Y')
		payload = {}
		for counter, task in enumerate(self.tasks):
			payload[f'{task}'] = f'tasks.task.history.list?taskId={task}&order[CREATED_DATE]=asc&filter[field]=TIME_SPENT_IN_LOGS&filter[>CREATED_DATE]={history_from_date}&filter[<CREATED_DATE]={history_to_date}'
			# print(payload[f'{task}'])
			if not self.task_data.get(task):
				self.task_data[task] = []
			if (counter + 1) % 50 == 0:
				response = requests.post(self.form_url(BATCH_HOOK), json={"cmd": payload})
				if response.status_code != http.HTTPStatus.OK:
					raise Exception(response.content)
				for task_id, chunk in response.json()['result']['result'].items():
					for entity in chunk['list']:
						user = entity['user']
						full_name = ' '.join((user['lastName'], user['name']))
						self.task_data[int(task_id)].append({
							'user_id': int(user['id']),
							'full_name': full_name,
							'email': user['login'],
							# 'date': str(datetime.strptime(entity['createdDate'], "%d.%m.%Y %H:%M:%S").date()),
							'date': format_new_date(entity['createdDate']),
							'time': floor(int(entity['value']['to']) - int(entity['value']['from']))
						})
				payload = {}

	def merge_data(self):
		for project_id, tasks in self.data.items():
			for index, task in enumerate(tasks):
				task['time'] = self.task_data[task['id']]
				self.data[project_id][index] = task

			self.output.append({
				'project_name': self.project_id2name[int(project_id)],
				'tasks': tasks
			})

	def execute(self):
		self.determine_task_ids()
		self.get_tasks()
		self.get_task_work_time()
		self.merge_data()
		with open('file.json', 'w', encoding='utf-8') as f:
			f.write(json.dumps(self.output, ensure_ascii=False, indent=2, default=str))
