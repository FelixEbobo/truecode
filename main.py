from datetime import datetime
from bitrix_collector import BitrixCollector
from excel_writer import ExcelCreator
from yandex_api import create_folder, publish_folder, put_report, get_public_url


def main():
	# from_date = '2020-10-24'
	# to_date = '2022-10-24'
	from_date = '2022-09-01'
	to_date = '2022-10-12'
	app = BitrixCollector(from_date, to_date)
	app.execute()
	# first_report_name = f'human_report_{from_date}_to_{to_date}.xlsx'
	# ExcelCreator().create_human_report('file.json', first_report_name)
	second_report_name = f'analytic_report_{from_date}_to_{to_date}.xlsx'
	ExcelCreator().create_analytics_report('file.json', second_report_name)

	# date = 'Отчёты_от_' + datetime.now().strftime('%d-%m-%Y_%H-%m-%S')
	# create_folder(date)
	# put_report(first_report_name, date)
	# put_report(second_report_name, date)
	# publish_folder(date)
	# url = get_public_url(date)
	# print(url)


if __name__ == '__main__':
	main()
